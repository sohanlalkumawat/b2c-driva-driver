package com.cars24.b2c.driva.service;

import com.cars24.auth.lib.model.JwtAuthenticationToken;
import com.cars24.b2c.driva.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
public class UserService {

    /**
     * method to get the user information from the jwt token, which is used for authentication purpose
     */
    public User getLoggedInUserInfo() {
        JwtAuthenticationToken authenticationToken = (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        if (!ObjectUtils.isEmpty(authenticationToken.getAuth().getPersonDetails())) {
            log.debug("user personal details {}",authenticationToken.getAuth().getPersonDetails());
            log.debug("user Auth {}",authenticationToken.getAuth());
            User user=new User();
            user.setUserId(authenticationToken.getAuth().getUid());
            user.setName(authenticationToken.getAuth().getPersonDetails().getName());
            user.setMobilePhone(authenticationToken.getAuth().getPersonDetails().getMobilePhone());
            user.setEmail(authenticationToken.getAuth().getPersonDetails().getEmail());
            return user;
        }
        throw new RuntimeException("User not logged in");
    }
}
