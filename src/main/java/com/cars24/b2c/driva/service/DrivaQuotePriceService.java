package com.cars24.b2c.driva.service;

import com.cars24.b2c.driva.client.DrivaClient;
import com.cars24.b2c.driva.dao.entity.DrivaQuotePrice;
import com.cars24.b2c.driva.dao.repository.DrivaQuotePriceDaoService;
import com.cars24.b2c.driva.domain.LoanOfferRequest;
import com.cars24.b2c.driva.domain.LoanOfferResponse;
import com.cars24.b2c.driva.domain.driva.DrivaQuotePriceResponse;
import com.cars24.b2c.driva.mapper.DrivaQuotePriceMapper;
import com.cars24.b2c.driva.mapper.DrivaQuotePriceRequestMapper;
import com.cars24.b2c.driva.mapper.LoanOfferMapper;
import com.cars24.b2c.driva.validator.LoanOfferRequestValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/*Manage the customer loan offer with driva quote price*/
@Slf4j
@Service
public class DrivaQuotePriceService {
    @Autowired
    LoanOfferRequestValidator loanOfferRequestValidator;
    @Autowired
    DrivaQuotePriceDaoService drivaQuotePriceDaoService;
    @Autowired DrivaClient drivaClient;
    @Autowired
    DrivaQuotePriceMapper drivaQuotePriceMapper;
    @Autowired
    LoanOfferMapper loanOfferMapper;
    @Autowired
    DrivaQuotePriceRequestMapper drivaRequestMapper;

    private String apikey="x";
    private String partnerId="x";

    /*Step-1 -> validate the request
    * Step-2 -> build/update the existing record on the basis of orderId/userId- save/update
    * Step-3 -> call the driva according the submitted flag- true
    * Step-4 -> update the collection and return the loanOffer response to user
    * */
    public LoanOfferResponse createLoan(LoanOfferRequest loanOfferRequest, String userId, String orderId, String country, String vehicleType){
        loanOfferRequestValidator.validateRequest(loanOfferRequest);
        DrivaQuotePrice drivaQuotePrice1=null;
        Optional<DrivaQuotePrice> drivaQuotePrice= drivaQuotePriceDaoService.findByUserIdAndOrderId(userId,orderId);
        if(!drivaQuotePrice.isPresent()){
            //build the drivaQuotePrice collection
             drivaQuotePrice1= drivaQuotePriceMapper.mapDrivaQuotePrice(loanOfferRequest);
        }
        else{
            // i will map not null property of loanOfferRequest in DrivaQuotePrice.
             drivaQuotePrice1= drivaQuotePriceMapper.mapDrivaQuotePrice(loanOfferRequest,drivaQuotePrice.get());
        }
         drivaQuotePrice1= drivaQuotePriceDaoService.saveOrUpdate(drivaQuotePrice1);
        if(loanOfferRequest.isSubmitted()){
           DrivaQuotePriceResponse drivaQuotePriceResponse= drivaClient.createQuotePrice(apikey,partnerId,drivaRequestMapper.mapDrivaQuotePriceRequest(drivaQuotePrice1));
            drivaQuotePrice1.setDrivaQuotePrice(drivaQuotePriceResponse);
            drivaQuotePrice1= drivaQuotePriceDaoService.saveOrUpdate(drivaQuotePrice1);
        }
        return loanOfferMapper.mapLoanOffer(drivaQuotePrice1);
    }

    /*Step-1 -> validate the request
     * Step-2 -> update the existing record on the basis of orderId/userId- update
     * Step-3 -> call the driva according the submitted flag- true
     * Step-4 -> update the collection and return the loanOffer response to user
     * */
    public LoanOfferResponse updateLoan(LoanOfferRequest loanOfferRequest, String userId, String orderId, String country, String vehicleType){
        // Get on the basis of UserId,orderId,country - and update not null request parameter I will update
        loanOfferRequestValidator.validateRequest(loanOfferRequest);
        DrivaQuotePrice drivaQuotePrice1=null;
        //todo need to send exception if collection not present
        Optional<DrivaQuotePrice> drivaQuotePrice= drivaQuotePriceDaoService.findByUserIdAndOrderId(userId,orderId);

        // i will map not null property of loanOfferRequest in DrivaQuotePrice.
        drivaQuotePrice1= drivaQuotePriceMapper.mapDrivaQuotePrice(loanOfferRequest,drivaQuotePrice.get());
        drivaQuotePrice1= drivaQuotePriceDaoService.saveOrUpdate(drivaQuotePrice1);

        if(loanOfferRequest.isSubmitted()){
            DrivaQuotePriceResponse drivaQuotePriceResponse= drivaClient.createQuotePrice(apikey,partnerId,drivaRequestMapper.mapDrivaQuotePriceRequest(drivaQuotePrice1));
            drivaQuotePrice1.setDrivaQuotePrice(drivaQuotePriceResponse);
            drivaQuotePrice1= drivaQuotePriceDaoService.saveOrUpdate(drivaQuotePrice1);
        }
        return loanOfferMapper.mapLoanOffer(drivaQuotePrice1);
    }
}
