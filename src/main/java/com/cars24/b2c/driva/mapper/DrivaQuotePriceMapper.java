package com.cars24.b2c.driva.mapper;

import com.cars24.b2c.driva.dao.entity.DrivaQuotePrice;
import com.cars24.b2c.driva.domain.LoanOfferRequest;
import org.springframework.stereotype.Component;

@Component
public class DrivaQuotePriceMapper {

    public DrivaQuotePrice mapDrivaQuotePrice(LoanOfferRequest loanOfferRequest){
        return new DrivaQuotePrice();
    }

    public DrivaQuotePrice mapDrivaQuotePrice(LoanOfferRequest loanOfferRequest,DrivaQuotePrice drivaQuotePrice){
        //to do map only not null propetry of loanOfferRequest to drivaQuotePrice
        return drivaQuotePrice;
    }
}
