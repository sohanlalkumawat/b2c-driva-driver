package com.cars24.b2c.driva.mapper;

import com.cars24.b2c.driva.dao.entity.DrivaQuotePrice;
import com.cars24.b2c.driva.domain.driva.DrivaLoanRequest;
import com.cars24.b2c.driva.domain.driva.DrivaQuotePriceRequest;
import org.springframework.stereotype.Component;

@Component
public class DrivaQuotePriceRequestMapper {

    public DrivaQuotePriceRequest mapDrivaQuotePriceRequest(DrivaQuotePrice drivaQuotePrice){

        return new DrivaQuotePriceRequest();
    }
}
