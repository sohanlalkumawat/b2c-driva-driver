package com.cars24.b2c.driva.domain;

import lombok.Data;

import java.util.List;

@Data
public class CustomerDetails {
    private String firstName;
    private String lastName;
    private String mobile;
    private String email;
    private String dateOfBirth;
    private String livingSituation;
    private CustomerAddress address;
    private CustomerAddress previousAddress;
    private List<CustomerEmploymentDetails> employment;
}
