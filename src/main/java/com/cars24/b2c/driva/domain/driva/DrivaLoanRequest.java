package com.cars24.b2c.driva.domain.driva;

import lombok.Data;

@Data
public class DrivaLoanRequest {
    private int amount;
    private int deposit;
    private int term;
}
