package com.cars24.b2c.driva.domain.driva;

import lombok.Data;

@Data
public class DrivaEmploymentRequest {
    private String type;
    private int durationYears;
    private int durationMonths;
    private boolean isCurrent;
}
