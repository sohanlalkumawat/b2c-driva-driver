package com.cars24.b2c.driva.domain;

import lombok.Data;

@Data
public class LoanOfferRequest {
    private String appointmentId;
    private CustomerDetails user;
    private CustomerLoanDetails loan;
    private boolean submitted;

}
