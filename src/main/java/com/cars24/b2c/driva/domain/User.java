package com.cars24.b2c.driva.domain;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/*
 * User entity stores information of an authenticated user
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    private String country;
    private String mobilePhone;
    private String name;
    private String email;
    private int id;
    @JsonAlias({"sub", "userId"})
    private String userId;

}

