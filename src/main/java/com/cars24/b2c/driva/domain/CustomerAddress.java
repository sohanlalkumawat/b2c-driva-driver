package com.cars24.b2c.driva.domain;

import lombok.Data;

@Data
public class CustomerAddress {
    private String full;
    private String unit;
    private String streetNumber;
    private String street;
    private String suburb;
    private String postCode;
    private String state;
}
