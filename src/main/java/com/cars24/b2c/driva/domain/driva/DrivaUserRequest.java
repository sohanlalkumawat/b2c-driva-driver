package com.cars24.b2c.driva.domain.driva;

import lombok.Data;
import java.util.List;

@Data
public class DrivaUserRequest {
    private String firstName;
    private String lastName;
    private String mobile;
    private String email;
    private String dateOfBirth;
    private String livingSituation;
    private DrivaAddressRequest address;
    private DrivaAddressRequest previousAddress;
    private List<DrivaEmploymentRequest> employment;
}
