package com.cars24.b2c.driva.domain;

import lombok.Data;
import java.util.List;

@Data
public class LoanOfferResponse {

    private String orderId;
    private String appointmentId;
    private List<LoanOfferDetails> loanOffer;

    @Data
    public class LoanOfferDetails {
        private String offerId;
        private String displayName;
        private String logo;
        private boolean eligible;
        private double interestRate;
        private double comparisonRate;
        private double monthly;
        private int term;
        private Double terminationFees;
        private String applyLink;
    }
}
