package com.cars24.b2c.driva.domain.driva;

import lombok.Data;

@Data
public class DrivaQuotePriceRequest {
    private DrivaUserRequest user;
    private DrivaLoanRequest loan;
    private String returnType="redirect";
}
