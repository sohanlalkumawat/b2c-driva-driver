package com.cars24.b2c.driva.domain;

import lombok.Data;

@Data
public class CustomerEmploymentDetails {
  private String type;
  private int durationYears;
  private int durationMonths;
  private boolean isCurrent;
}
