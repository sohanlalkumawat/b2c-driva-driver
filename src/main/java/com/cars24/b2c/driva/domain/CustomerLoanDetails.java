package com.cars24.b2c.driva.domain;

import lombok.Data;

@Data
public class CustomerLoanDetails {
    private int amount;
    private int deposit;
    private int term;
}
