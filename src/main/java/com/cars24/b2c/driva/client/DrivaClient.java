package com.cars24.b2c.driva.client;

import com.cars24.b2c.driva.domain.driva.DrivaQuotePriceRequest;
import com.cars24.b2c.driva.domain.driva.DrivaQuotePriceResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

public interface DrivaClient {

// Create a new quote request and get pricing
    @PostMapping(
            value = "/v1/quote/price",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    DrivaQuotePriceResponse createQuotePrice(
            @RequestHeader(value = "x-api-key", required = true) String apiKey,
            @RequestHeader(value = "partnerId", required = true) String partnerId,
            @RequestBody DrivaQuotePriceRequest drivaQuotePriceRequest);

    //Update existing quote and get updated pricing results based on the new data.
    @PutMapping(
            value = "/v1/quote/price/{UUID}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    DrivaQuotePriceResponse updateQuotePrice(
            @PathVariable(name = "UUID") String UUID,
            @RequestHeader(value = "x-api-key", required = true) String apiKey,
            @RequestHeader(value = "partnerId", required = true) String partnerId,
            @RequestBody DrivaQuotePriceRequest drivaQuotePriceRequest);

    // todo read api key and partenerId from config


}
