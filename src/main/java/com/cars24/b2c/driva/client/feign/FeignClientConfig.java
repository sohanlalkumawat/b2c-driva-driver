package com.cars24.b2c.driva.client.feign;

import com.cars24.auth.lib.FeignClientInterceptor;
import com.cars24.b2c.driva.client.DrivaClient;
import feign.Target;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import feign.Client;
import feign.Feign;
import feign.Feign.Builder;

@Configuration
public class FeignClientConfig {

    @Autowired FeignClientInterceptor feignClientInterceptor;

    @Bean
    public Builder getClientBuilder(ObjectFactory<HttpMessageConverters> messageConverters,
                                    Client client) {
        return Feign.builder().client(client).encoder(new SpringEncoder(messageConverters))
                .decoder(new SpringDecoder(messageConverters)).contract(new SpringMvcContract())
                .errorDecoder(new FeignErrorDecoder());

    }

    @Bean
    public DrivaClient drivaClient(
            @Value("${driva.service.url}") String clientBasePath, Builder clientBuilder) {
        clientBuilder.requestInterceptor(feignClientInterceptor);
        return clientBuilder.target(new Target.HardCodedTarget<>(DrivaClient.class, clientBasePath));
    }

}
