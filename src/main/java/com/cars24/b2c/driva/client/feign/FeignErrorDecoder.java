package com.cars24.b2c.driva.client.feign;

import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FeignErrorDecoder extends ErrorDecoder.Default {

    @Override
    public Exception decode(final String methodKey, final Response response) {
        //log.error("FeignErrorDecoder Inter-Service error for methodKey {} request {}",methodKey,response.request());
        try {
            if (response.body() != null) {
                log.error("FeignErrorDecoder Inter-Service error for methodKey {} request {} status {} reason {} and body {} ",methodKey,response.request(),response.status(),response.reason(),Util.toString(response.body().asReader(StandardCharsets.UTF_8)));
                return new RuntimeException("service communication exception for "+methodKey+" and status "+response.status()+" reason "+response.reason());
            }
        } catch (IOException e) {
            return new RuntimeException(e.getMessage(),e);
        }
        return new RuntimeException("Unexpected exception during the inter-service call for methodKey "+methodKey);
    }
}
