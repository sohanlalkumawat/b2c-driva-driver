package com.cars24.b2c.driva.dao;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.annotation.EnableArangoRepositories;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;


@Configuration
@EnableConfigurationProperties(ArangodbConfiguration.ArangodbProperties.class)
@EnableArangoRepositories(basePackages = {"com.cars24.b2c.driva.dao"})
@Slf4j
public class ArangodbConfiguration implements com.arangodb.springframework.config.ArangoConfiguration {
    @Autowired
    private ArangodbProperties arangodbProperties;

    @Override
    public ArangoDB.Builder arango() {
        ArangoDB.Builder builder = new ArangoDB.Builder()
                .host(arangodbProperties.getHost(), arangodbProperties.getPort())
                .user(arangodbProperties.getUsername())
                .password(arangodbProperties.getPassword())
                .keepAliveInterval(arangodbProperties.getKeepAliveInterval())
                .timeout(arangodbProperties.getTimeout()*1000)
                .maxConnections(arangodbProperties.getMaxConnection());

        if (!StringUtils.isEmpty(arangodbProperties.getCa())) {
            builder.useSsl(true).sslContext(createSslContext());
        }
        return builder;
    }

    @Override
    public String database() {
        return arangodbProperties.getDatabase();
    }

    private SSLContext createSslContext() {
        try (InputStream is = new java.io.ByteArrayInputStream(
                Base64.getDecoder().decode(arangodbProperties.getCa()))) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate caCert = (X509Certificate) cf.generateCertificate(is);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null);
            ks.setCertificateEntry("caCert", caCert);
            tmf.init(ks);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            return sslContext;
        } catch (Exception e) {
            log.error("Exception in createSslContext for arrangodb {}",e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
    @Getter
    @Setter
    @ConfigurationProperties(prefix = "arango")
    public class ArangodbProperties {
        private String host;
        private int port;
        private String username;
        private String password;
        private String database;
        private String ca;
        private int maxConnection;
        private int keepAliveInterval;
        private int timeout;
    }
}