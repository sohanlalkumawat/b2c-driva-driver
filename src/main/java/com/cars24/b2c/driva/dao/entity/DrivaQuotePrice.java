package com.cars24.b2c.driva.dao.entity;
import com.arangodb.springframework.annotation.Document;
import com.cars24.b2c.driva.domain.LoanOfferRequest;
import com.cars24.b2c.driva.domain.driva.DrivaQuotePriceResponse;
import org.springframework.data.annotation.Id;
import com.arangodb.springframework.annotation.PersistentIndex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * DrivaQuotePrice collection is manage to loanRequest and driva quote price details
 */
@PersistentIndex(fields = {"orderId"}, unique = true)
@PersistentIndex(fields = {"userId"})
@PersistentIndex(fields = {"appointmentId"})
@PersistentIndex(fields = {"vehicleType"})
@PersistentIndex(fields = {"status"})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "driva-quote-price")
public class DrivaQuotePrice {
    @Id
    private String id;
    private String userId;
    private String orderId;
    private String appointmentId;
    private String country;
    private String vehicleType;
    private LoanOfferRequest loanOfferRequest;
    private DrivaQuotePriceResponse drivaQuotePrice;
    /* represent financing current status -CREATED/APPROVED/COMPLETED/REJECTED */
    private String status;

    public enum QuotePriceStatus {
        CREATED,APPROVED,COMPLETED,REJECTED;
    }
    public enum VehicleType {
        CAR, BIKE;
    }
}
