package com.cars24.b2c.driva.dao.repository;

import com.cars24.b2c.driva.dao.entity.DrivaQuotePrice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class DrivaQuotePriceDaoService {

  @Autowired private DrivaQuotePriceRepository orderRepository;

  public DrivaQuotePrice saveOrUpdate(DrivaQuotePrice drivaQuotePrice){
    return orderRepository.save(drivaQuotePrice);
  }
  public Optional<DrivaQuotePrice> findByUserIdAndOrderId(String userId, String orderId){
    return orderRepository.findByUserIdAndOrderId(userId,orderId);
  }

}
