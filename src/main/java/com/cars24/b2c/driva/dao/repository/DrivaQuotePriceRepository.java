package com.cars24.b2c.driva.dao.repository;
import com.arangodb.springframework.repository.ArangoRepository;
import com.cars24.b2c.driva.dao.entity.DrivaQuotePrice;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DrivaQuotePriceRepository extends ArangoRepository<DrivaQuotePrice, String> {

     Optional<DrivaQuotePrice> findByUserIdAndOrderId(String userId,String orderId);
}
