package com.cars24.b2c.driva.utility;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class AppContextUtils implements ApplicationContextAware {

    private static ApplicationContext CONTEXT;

    public void setApplicationContext(ApplicationContext context) throws BeansException {
        CONTEXT = context;
    }

    public static <T> T getBean(Class<T> beanClass) {
        return CONTEXT.getBean(beanClass);
    }

    public static String activeProfile(){
        Environment environment=getBean(Environment.class);
        return environment.getActiveProfiles()[0];
    }

    public static boolean isProdProfileActive() {
        return "prod".equalsIgnoreCase(activeProfile());
    }
}
