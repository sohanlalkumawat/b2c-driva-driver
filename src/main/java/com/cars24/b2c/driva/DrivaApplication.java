package com.cars24.b2c.driva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrivaApplication {
    public static void main(String[] args) {
        SpringApplication.run(DrivaApplication.class, args);
    }
}