package com.cars24.b2c.driva.controller;

import com.cars24.b2c.driva.domain.LoanOfferRequest;
import com.cars24.b2c.driva.domain.LoanOfferResponse;
import com.cars24.b2c.driva.service.DrivaQuotePriceService;
import com.cars24.b2c.driva.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/*Manage the customer loan offer with driva quote price*/
@RequestMapping("/api/v1")
@RestController
public class DrivaQuotePriceController {

  @Autowired DrivaQuotePriceService drivaQuotePriceService;
  @Autowired UserService userService;

  /* Manage the customer loan create with driva quote price*/
  @PostMapping("/quote/price/{orderId}")
  public ResponseEntity<?> createLoan(
      @PathVariable String orderId,
      @RequestHeader(value = "X_COUNTRY") String country,
      @RequestHeader(value = "X_VEHICLE_TYPE") String vehicleType,
      @RequestBody LoanOfferRequest loanOfferRequest) {
    String userId = userService.getLoggedInUserInfo().getUserId();
    LoanOfferResponse loanOfferResponse=drivaQuotePriceService.createLoan(loanOfferRequest, userId, orderId, country.toUpperCase(), vehicleType.toUpperCase());

    return ResponseEntity.ok(loanOfferResponse);

  }
    /* Manage the customer loan update with driva quote price*/
  @PostMapping("/quote/price/{orderId}")
  public ResponseEntity<?> updateLoan(
      @PathVariable String orderId,
      @RequestHeader(value = "X_COUNTRY") String country,
      @RequestHeader(value = "X_VEHICLE_TYPE") String vehicleType,
      @RequestBody LoanOfferRequest loanOfferRequest) {
    String userId = userService.getLoggedInUserInfo().getUserId();
    LoanOfferResponse loanOfferResponse=drivaQuotePriceService.updateLoan(loanOfferRequest, userId, orderId, country.toUpperCase(), vehicleType.toUpperCase());
    return ResponseEntity.ok(loanOfferResponse);
  }
}
