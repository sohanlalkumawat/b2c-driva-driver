package com.cars24.b2c.driva;

import com.arangodb.ArangoDBException;
import com.cars24.b2c.driva.exception.ValidationException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class AppControllerAdvice extends ResponseEntityExceptionHandler {

  /*@ExceptionHandler(value = {OrderException.class})
  public ResponseEntity<String> handelOrderException(OrderException ex) {
    // log.error("Global Exception Handler with OrderException is {}", ex);
    return ResponseEntity.status(ex.getHttpStatus()).body(ex.getCode());
  }*/

  @ExceptionHandler(value = {ArangoDBException.class})
  public ResponseEntity<String> handelArrangoDBException(ArangoDBException ex) {
    log.error("Arango db Exception occurred {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body("Database server error, Please connect to support team");
  }

  @ExceptionHandler(value = {ValidationException.class})
  public ResponseEntity<String> handelArrangoDBException(ValidationException ex, @RequestBody String requestString) {
    log.error("ValidationException occurred for request {} and exception {}",requestString,ex);
    return ResponseEntity.status(ex.getHttpStatus())
            .body(ex.getMessage());
  }

  @ExceptionHandler(value = {Exception.class})
  public ResponseEntity<String> commonExceptionHandeler(Exception ex) {
    log.error("General exception comes  {}", ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body("There some technical issue, Please connect tech support");
  }

  @ExceptionHandler(value = {NullPointerException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public String nullPointerException(NullPointerException exception) {
    log.error("NullPointer exception occurred {} ", exception);
    return exception.getMessage();
  }

  @ExceptionHandler(value = {IllegalArgumentException.class, InvalidFormatException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public String handleGeneralException(Exception ex) {
    log.error("IllegalArgumentException or InvalidFormatException exception occurred {} ", ex);
    return ex.getMessage();
  }
}
