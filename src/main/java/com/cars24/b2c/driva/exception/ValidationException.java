package com.cars24.b2c.driva.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class ValidationException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message = "";
    public ValidationException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public ValidationException(HttpStatus httpStatus, String message,Throwable th) {
        super(th);
        this.httpStatus = httpStatus;
        this.message = message;
    }
}
